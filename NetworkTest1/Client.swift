//
//  Client.swift
//  NetworkTest1
//
//  Created by Jens Bliemeister on 17.06.23.
//

import Foundation
import Network

struct Client {
    var ethernetAddress: EthernetAddress
    var network: IPv4Address
}
