//
//  NetworkTest1App.swift
//  NetworkTest1
//
//  Created by Jens Bliemeister on 14.06.23.
//

import SwiftUI

@main
struct NetworkTest1App: App {
    @StateObject private var modelData = ModelData()
    
    var body: some Scene {
        WindowGroup {
            NetworkTestView()
        }
    }
    
    init() {
        // setup the model data
        
        
    }
}
