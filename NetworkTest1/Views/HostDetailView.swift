//
//  HostDetailView.swift
//  NetworkTest1
//
//  Created by Jens Bliemeister on 29.06.23.
//

import SwiftUI

struct HostDetailView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct HostDetailView_Previews: PreviewProvider {
    static var previews: some View {
        HostDetailView()
    }
}
