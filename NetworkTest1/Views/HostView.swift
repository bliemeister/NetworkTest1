//
//  HostView.swift
//  NetworkTest1
//
//  Created by Jens Bliemeister on 25.06.23.
//

import SwiftUI

struct HostView: View {
    var host: Host
    
    var body: some View {
        HStack {
            Image(systemName: "desktopcomputer")
                .font(.largeTitle)
                .imageScale(.medium)
            VStack(alignment: .leading) {
                Text(host.commonName)
                    .font(.title2)
                    .fontWeight(.bold)
                HStack {
                    Text(host.addressArray[0].description)
                    Spacer()
                    Text(host.ethernetAddress.description)
                }
            }
        }
    }
}

struct HostView_Previews: PreviewProvider {
    static var host = Host(commonName: "Mac Pro Arbeitszimmer", addressArray: [.ipv4(10, 64, 0, 31)], ethernetAddress: EthernetAddress(addressString: "00:1:22:33:ef:55")!)
        
    
    static var previews: some View {
        HostView(host: host)
    }
}
