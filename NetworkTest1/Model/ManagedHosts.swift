//
//  ManagedHosts.swift
//  NetworkTest1
//
//  Created by Jens Bliemeister on 24.06.23.
//

import Foundation

class ManagedHosts: Codable, ObservableObject {
    var hosts: [Host] = []
    
    func save() {
        let fileManager = FileManager.default
        
        do {
            let appSupportURL = try applicationSupportURL()
            
            if !fileManager.fileExists(atPath: appSupportURL.relativePath) {
                try fileManager.createDirectory(at: appSupportURL, withIntermediateDirectories: false)
            }
        } catch {
            print(error)
        }
        
    }
    
    func load() {
        
    }
    
    func applicationSupportURL() throws -> URL {
            guard let bundleIdentifier = Bundle.main.bundleIdentifier else {
                throw ManagedHostsError.nilBundleIdentifier
            }
            
            var applicationSupportURL = try FileManager.default.url(
                for: .applicationSupportDirectory,
                in: .userDomainMask,
                appropriateFor: nil,
                create: true
            )
            
            applicationSupportURL.append(path: bundleIdentifier, directoryHint: .isDirectory)
            
            return applicationSupportURL
    }
    
    enum ManagedHostsError: Error {
        case nilBundleIdentifier
    }
    
}
