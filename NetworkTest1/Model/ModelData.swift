//
//  ModelData.swift
//  NetworkTest1
//
//  Created by Jens Bliemeister on 30.06.23.
//

import Foundation

final class ModelData: ObservableObject {
    @Published var managedHosts: [Host]
    
    init(managedHosts: [Host] = []) {
        self.managedHosts = managedHosts
    }
    
    convenience init(fromURL: URL) {
        self.init()
    }
}


#if DEBUG
extension ModelData {
    func generateDemoModel() {
        let addressArray1 = 
        let host1 = Host(commonName: <#T##String#>, addressArray: <#T##[Host.AddressType]#>, ethernetAddress: <#T##EthernetAddress#>)
    }
    
    func writeDemoModel(to url: URL? = nil) {
        let url = url ?? URL.downloadsDirectory.appendingPathComponent("ManagedHostsDemo.plist")
        
        let encoder = PropertyListEncoder()
        encoder.outputFormat = .xml
        
        do {
            let data = try encoder.encode(managedHosts)
            
            try data.write(to: url)
            
        } catch {
            print(error)
        }
    }
    
    static func loadDemoModel(from url: URL? = nil) -> [Host]? {
        let url = url ?? URL.downloadsDirectory.appendingPathComponent("ManagedHostsDemo.plist")
        
        let decoder = PropertyListDecoder()
        
        do {
            let path = url.path()
            if FileManager.default.isReadableFile(atPath: path) {
                guard let data = FileManager.default.contents(atPath: path) else { return nil }
                
                return try decoder.decode([Host].self, from: data)
            }
        } catch {
            print(error)
            
            return nil
        }
    }
}
#endif
