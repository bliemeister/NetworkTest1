//
//  EthernetAddress.swift
//  NetworkTest1
//
//  Created by Jens Bliemeister on 17.06.23.
//

import Foundation
import Network

struct EthernetAddress: Codable, CustomStringConvertible {
    var description: String {
        address.map { String(format: "%2x", $0) }
            .joined(separator: ":")
    }
    
    var address: [UInt8]
    
    var nwEthernetAddress: nw_ethernet_address_t {
        (address[0], address[1], address[2], address[3], address[4], address[5])
    }
    
    var data: Data {
        Data(address)
    }
    
    init(_ num1: UInt8, _ num2: UInt8, _ num3: UInt8,
         _ num4: UInt8, _ num5: UInt8, _ num6: UInt8) {
        self.address = [num1, num2, num3, num4, num5, num6]
    }
    
    init(nwEthernetAddress nwnum: nw_ethernet_address_t) {
        self.address = [nwnum.0, nwnum.1, nwnum.2, nwnum.3, nwnum.4, nwnum.5]
    }
    
    init?(addressString: String, separator: String = ":") {
        let stringArray = addressString.split(separator: separator)
        
        if stringArray.count != 6 { return nil }
        
        let numberArray = stringArray.map { UInt8($0, radix: 16) }
        
        if numberArray.contains(where: { $0 == nil }) { return nil }
        else {
            self.address = numberArray.map { $0! }
        }
    }
}
