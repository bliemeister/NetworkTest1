//
//  Host.swift
//  NetworkTest1
//
//  Created by Jens Bliemeister on 23.06.23.
//

import Foundation

struct Host: Codable, Identifiable {
    enum AddressType: Codable, CustomStringConvertible {
        case dnsName(String)
        case ipv4(UInt8, UInt8, UInt8, UInt8)
        case ipv6(UInt16, UInt16, UInt16, UInt16, UInt16, UInt16, UInt16, UInt16)
        
        var description: String {
            get {
                switch self {
                    case .dnsName(let name):
                        return name
                    
                    case .ipv4(let addr1, let addr2, let addr3, let addr4):
                        return String("\(addr1).\(addr2).\(addr3).\(addr4)")
                    
                    case .ipv6(let addr0, let addr1, let addr2, let addr3,
                               let addr4, let addr5, let addr6, let addr7):
                        let address: [UInt16] = [addr0, addr1, addr2, addr3,
                                                 addr4, addr5, addr6, addr7]
                        
                        var addressString = ""
                        
                        // TODO: Kürzung der Adresse bei repetitiven 0-Werten
                        // ergibt longestSequenceIndex und longestSequenceLength
                        // dies wird im nächsten Schritt aus dem Array entfernt
                        var longestSequenceIndex = 0
                        var longestSequenceLength = 0
                        
                        var lastZeroIndex: Int = 0
                        var readZero = false
                        
                        for (index, value) in address.enumerated() {
                            if value == 0 {
                                if !readZero {
                                    lastZeroIndex = index
                                    readZero = true
                                }
                                
                                if index == address.endIndex - 1 {
                                    let currentLength = address.distance(from: lastZeroIndex, to: index) + 1
                                    if  currentLength > longestSequenceLength {
                                        longestSequenceIndex = lastZeroIndex
                                        longestSequenceLength = currentLength
                                    }
                                }
                            } else {
                                if readZero {
                                    let currentLength = address.distance(from: lastZeroIndex, to: index)
                                    if currentLength > longestSequenceLength {
                                        longestSequenceIndex = lastZeroIndex
                                        longestSequenceLength = currentLength
                                        readZero = false
                                    }
                                }
                            }
                        }
                        
                        if longestSequenceLength > 0 {
                            let firstPart = address.prefix(upTo: longestSequenceIndex)
                            let firstPartStrings = firstPart.map { String(format: "%x", $0) }
                            let firstPartString = firstPartStrings.reduce("") { (result, element) in
                                result + element + ":"
                            }
                            
                            var secondPartString: String
                            if address.index(longestSequenceIndex, offsetBy: longestSequenceLength) == address.endIndex {
                                secondPartString = ":"
                            } else {
                                let index = address.index(longestSequenceIndex, offsetBy: longestSequenceLength)
                                let secondPart = address.suffix(from: index)
                                let secondPartStrings = secondPart.map { String(format: "%x", $0) }
                                secondPartString = secondPartStrings.reduce("") { (result, element) in
                                    result + ":" + element
                                }
                            }
                            addressString = firstPartString + secondPartString
                            
                        } else {
                            addressString = String(format: "%x:%x:%x:%x:%x:%x:%x:%x",
                                                   addr0, addr1, addr2, addr3,
                                                   addr4, addr5, addr6, addr7)
                        }
                        
                        return addressString
                }
            }
        }
    }
    
    var id = UUID()
    
    var commonName: String
    var addressArray: [AddressType]
    
    var ethernetAddress: EthernetAddress
}
